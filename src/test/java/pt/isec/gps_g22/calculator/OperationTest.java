package pt.isec.gps_g22.calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OperationTest
{
    @Test
    public void div()
    {
        assertEquals(0.5, iOperation.operation(eOperation.DIV, 1, 2));
    }


    @Test
    public void mult()
    {
        assertEquals(2, iOperation.operation(eOperation.MULT, 1, 2));
    }

    @Test
    public void sum()
    {
        assertEquals(3, iOperation.operation(eOperation.SUM, 1, 2));
    }

    @Test
    public void minus()
    {
        assertEquals(-1, iOperation.operation(eOperation.MINUS, 1, 2));
    }

    @Test
    public void sqrt()
    {
        assertEquals(2, iOperation.operation(eOperation.SQRT, 4, 1));
    }

    @Test
    public void fact()
    {
        assertEquals(24, iOperation.operation(eOperation.FACT, 4, 0));
    }
}