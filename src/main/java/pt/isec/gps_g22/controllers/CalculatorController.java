package pt.isec.gps_g22.controllers;

import javafx.fxml.FXML;

import javafx.event.ActionEvent;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import pt.isec.gps_g22.calculator.eOperation;
import pt.isec.gps_g22.calculator.iOperation;

public class CalculatorController
{
    private static double result = 0;
    private static float current_value = 0;
    private static eOperation operator = eOperation.ANY;

    @FXML
    private TextField display;

    String getButtonValue(ActionEvent event){
        Button clickedButton = (Button) event.getSource();
        return clickedButton.getText();
    }
    @FXML
    public void onNumberButtonClick(ActionEvent actionEvent) {
        display.setText(display.getText() + getButtonValue(actionEvent));
    }

    @FXML
    public void onOperatorButtonClick(ActionEvent actionEvent) {
        try {
            current_value = Float.parseFloat(display.getText());
        }catch (NumberFormatException e){
            display.setText("Invalid operation!");
            return;
        }

        switch(getButtonValue(actionEvent)){
            case "+" -> operator = eOperation.SUM;
            case "-" -> operator = eOperation.MINUS;
            case "/" -> operator = eOperation.DIV;
            case "*" -> operator = eOperation.MULT;
            case "!" -> operator = eOperation.FACT;
            case "SQRT" -> operator = eOperation.SQRT;
        }

        display.clear();
    }

    @FXML
    public void onOperatorEquals(ActionEvent actionEvent) {
        try {
            float secondOperand = 0;
            if(operator != eOperation.FACT && operator != eOperation.SQRT){
                secondOperand= Float.parseFloat(display.getText());
            }

            result = iOperation.operation(operator, current_value, secondOperand);
            System.out.println(result);

            display.setText(Double.toString(result));

        }catch (NumberFormatException e){
            display.setText("Invalid operation!");

        }finally {
            operator = eOperation.ANY;
            current_value = 0;
        }

    }

    @FXML
    public void onOperationClear(ActionEvent actionEvent) {
        display.clear();
        result = 0;
        current_value = 0;
        operator = eOperation.ANY;
    }
}