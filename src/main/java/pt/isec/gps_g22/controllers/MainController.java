package pt.isec.gps_g22.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pt.isec.gps_g22.CalculatorApplication;

import java.io.IOException;
import java.util.Objects;

public class MainController
{
    @FXML
    private VBox box;

    @FXML
    private ComboBox<String> menu;

    public void onModeChange() throws IOException
    {
        String mode_path = null;
        String mode = menu.getValue();
        switch(mode)
        {
            case "Calculator" -> mode_path = "calculator-view.fxml";
            case "Number Converter" -> mode_path = "num-converter-view.fxml";
            case "Date calculation" -> mode_path = "date-calculation.fxml";
            case "Volume of a cylinder" -> mode_path = "cylinder-volume.fxml";
            case "Volume of a cone" -> mode_path = "cone-volume.fxml";
        }

        assert mode_path != null;
        GridPane calculator_pane = FXMLLoader.load(Objects.requireNonNull(CalculatorApplication.class.getResource(mode_path)));

        box.getChildren().remove(1);
        box.getChildren().add(calculator_pane);
    }
}
