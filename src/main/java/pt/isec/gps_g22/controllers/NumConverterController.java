package pt.isec.gps_g22.controllers;

import javafx.fxml.FXML;

import javafx.event.ActionEvent;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class NumConverterController
{
    @FXML
    private TextField display_hex, display_dec;

    private int display_type = 0;

    private void setDisplay(int type)
    {
        display_type = type;
    }

    @FXML
    public void initialize()
    {
        display_dec.setOnMouseClicked(e -> setDisplay(1));
        display_hex.setOnMouseClicked(e -> setDisplay(0));
    }

    private void setTextDisplay(String text)
    {
        switch(display_type)
        {
            case 0 -> display_hex.setText(text);
            case 1 -> display_dec.setText(text);
        }
    }
    
    private String getTextDisplay()
    {
        return switch(display_type)
        {
            case 0 -> display_hex.getText();
            case 1 -> display_dec.getText();
            default -> "";
        };
    }

    private void convertTextDisplay()
    {
        String text = getTextDisplay();
        if (text.isEmpty())
            return;

        switch(display_type)
        {
            case 0 -> display_dec.setText(String.valueOf(Long.parseUnsignedLong(text, 16)));
            case 1 -> display_hex.setText(Long.toHexString(Long.parseUnsignedLong(text)));
        }
    }
    
    String getButtonValue(ActionEvent event)
    {
        Button clickedButton = (Button) event.getSource();
        return clickedButton.getText();
    }

    @FXML
    public void onNumberButtonClick(ActionEvent actionEvent)
    {
        setTextDisplay(getTextDisplay() + getButtonValue(actionEvent));
        convertTextDisplay();
    }

    @FXML
    public void onLetterButtonClick(ActionEvent actionEvent)
    {
        if (display_type == 1)
            return;

        setTextDisplay(getTextDisplay() + getButtonValue(actionEvent));
        convertTextDisplay();
    }

    @FXML
    public void onOperationClear(ActionEvent actionEvent)
    {
        display_dec.clear();
        display_hex.clear();
    }
}
