package pt.isec.gps_g22.controllers;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;


public class DateCalculationController {

    public ComboBox menu;
    public RadioButton addRadioButton, subtractRadioButton;
    public DatePicker datePicker1, datePicker2;
    public HBox yearsMonthsDaysHBox;
    public TextField textField1, textField2, textField3;
    public Label resultLabel;

    private void addNumericRestriction(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }
    private void setVisibleProperty(Node node, String str){
        node.visibleProperty().bind(menu.valueProperty().isEqualTo(str));
    }
    public void initialize() {
        addNumericRestriction(textField1);
        addNumericRestriction(textField2);
        addNumericRestriction(textField2);
        setVisibleProperty(addRadioButton,"Add or Subtract Days");
        setVisibleProperty(subtractRadioButton,"Add or Subtract Days");
        setVisibleProperty(yearsMonthsDaysHBox,"Add or Subtract Days");
        setVisibleProperty(datePicker2,"Date Difference");
    }
    public void onDatePickerChange() {
        if (datePicker1.getValue() == null) return;
        if (menu.getValue().equals("Date Difference")) {
            if (datePicker2.getValue() == null) return;
            Period period = Period.between(datePicker1.getValue(), datePicker2.getValue());
            resultLabel.setText("Result: " + period.getYears() + " years, " + period.getMonths() + " months, " + period.getDays() + " days");
        }else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd'/'MM'/'yyyy");
            long days=Long.parseLong(textField3.getText()),months=Long.parseLong(textField2.getText()),years=Long.parseLong(textField1.getText());

            if(subtractRadioButton.isSelected()) {
                LocalDate date = datePicker1.getValue().minusYears(years).minusMonths(months).minusDays(days);
                resultLabel.setText("Result: " + formatter.format(date));
            }else if(addRadioButton.isSelected()) {
                LocalDate date = datePicker1.getValue().plusYears(years).plusMonths(months).plusDays(days);
                resultLabel.setText("Result: " + formatter.format(date));
            }
        }
    }
}
