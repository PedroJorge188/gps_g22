package pt.isec.gps_g22.calculator;

public enum eOperation {
    SUM,
    MULT,
    DIV,
    MINUS,
    SQRT,
    FACT,

    ANY
}
