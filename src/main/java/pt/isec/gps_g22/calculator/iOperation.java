package pt.isec.gps_g22.calculator;
import java.lang.Math;

public interface iOperation
{
    static double operation(eOperation operator, double firstValue, double secondValue)
    {
        return switch (operator)
        {
            case DIV -> firstValue / secondValue;
            case MULT -> firstValue * secondValue;
            case SUM -> firstValue + secondValue;
            case MINUS -> firstValue - secondValue;
            case FACT -> {
                long fact = 1;
                for (int i = 2; i <= firstValue; i++) {
                    fact = fact * i;
                }
                yield fact;
            }
            case SQRT -> Math.sqrt(firstValue);
            default -> 0;
        };
    }
}